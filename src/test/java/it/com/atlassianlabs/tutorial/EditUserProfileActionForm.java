package it.com.atlassianlabs.tutorial;

import com.atlassian.pageobjects.elements.CheckboxElement;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.plugin.automation.page.ModuleKey;
import com.atlassian.plugin.automation.page.action.ActionForm;
import org.openqa.selenium.By;

// The only trick here is the usage of the ModuleKey annotation to get the full module key to the tested automation action (value in the Select action selectbox)
@ModuleKey("com.my.plugin:edit-user-profile-action")
public class EditUserProfileActionForm extends ActionForm
{
    public EditUserProfileActionForm(PageElement container, String moduleKey)
    {
        super(container, moduleKey);
    }

    public EditUserProfileActionForm propertyKey(final String propertyKey)
    {
        setActionParam("editUserProfileKeyField", propertyKey);
        return this;
    }

    public EditUserProfileActionForm propertyValue(final String propertyValue)
    {
        setActionParam("editUserProfileValueField", propertyValue);
        return this;
    }

    public EditUserProfileActionForm shouldOverwrite(final boolean shouldOverwrite)
    {
        final CheckboxElement checkbox = container.find(By.name("editUserProfileShouldOverwrite"), CheckboxElement.class);
        if (shouldOverwrite)
        {
            checkbox.check();
        }
        else
        {
            checkbox.uncheck();
        }
        return this;
    }
}