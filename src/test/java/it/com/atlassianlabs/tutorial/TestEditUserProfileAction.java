package it.com.atlassianlabs.tutorial;

import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.tests.TestBase;
import com.atlassian.plugin.automation.page.ActionsForm;
import com.atlassian.plugin.automation.page.AdminPage;
import com.atlassian.plugin.automation.page.TriggerPage;
import com.atlassian.plugin.automation.page.trigger.IssueEventTriggerForm;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestEditUserProfileAction extends TestBase
{
    @Before
    public void setup()
    {
        backdoor().restoreBlankInstance();
        jira().quickLoginAsSysadmin();
    }
    @After
    public void teardown()
    {
        // don't forget to cleanup all automation rules
        jira().quickLoginAsSysadmin();
        final AdminPage automationAdmin = jira().goTo(AdminPage.class);
        automationAdmin.deleteAllRules();
    }
    @Test
    public void testEditUserPropertiesActionWithOverwrite()
    {
        AdminPage automationAdmin = jira().goTo(AdminPage.class);
        final TriggerPage triggerPage = automationAdmin.addRuleForm().enabled(true).ruleName("test").ruleActor("admin").next();
        triggerPage.selectTrigger(IssueEventTriggerForm.class).setEvents("Issue Commented");
        ActionsForm actionsForm = triggerPage.next();
        actionsForm.setInitialAction(EditUserProfileActionForm.class).propertyKey("jira.user.timezone").propertyValue("Asia/Tokyo").shouldOverwrite(true);
        actionsForm.next().save();
        CustomViewProfilePage profilePage = jira().goTo(CustomViewProfilePage.class);
        assertFalse("Profile shouldn't contain Tokyo", profilePage.getTimezone().contains("Tokyo"));
        final IssueCreateResponse createResponse = jira().backdoor().issues().createIssue("HSP", "test admin issue");
        jira().backdoor().issues().commentIssue(createResponse.key(), "test");
 
        profilePage = jira().goTo(CustomViewProfilePage.class);
        assertTrue("Profile should contain Tokyo", profilePage.getTimezone().contains("Tokyo"));
    }
}
